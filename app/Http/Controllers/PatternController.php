<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;


class PatternController extends BaseController
{
    public function index(Request $request){
        $length = $request->get('length');
        $pattern = [];

        //TEMPLATE
        $tmp = [];
        for ($i = 0; $i < $length; $i++) {
            for ($j = 0; $j < $length; $j++) {
                $tmp[$i][$j] = "@";
            }
        }

        $pattern[] = self::pattern1($length, $tmp);
        $pattern[] = self::patternSpiral($length, $tmp);
        $pattern[] = self::patternSpiral($length, $tmp, true);
        $pattern[] = self::patternNested($length, $tmp);

        return response()->json($pattern);
    }

    function checkRow(&$r, &$c, $isRow, $isPlus, $v) {
        if ($isRow) {
            $r = $isPlus ? ($r + $v) : ($r - $v);
        } else {
            $c = $isPlus ? ($c + $v) : ($c - $v);
        }
    }

    function pattern1($length, $tmp) {
        //PATTERN 1
        $p1 = $tmp;
        $startRow = 0;
        $startCol = 1;
        $endRow = $length - 1;
        $endCol = $length - 2;

        $forward = true;
        while ($startRow != $endRow || $startCol != $endCol) {
            $p1[$startRow][$startCol] = " ";
            if ($forward) {
                $startRow += 1;
                $forward = false;
            } else {
                $left = ($startCol == 1);
                for($i = $startCol; $left ? $i < $length - 1 : $i > 0; $left ? $i++ : $i--) {
                    $startCol = $i;
                    $p1[$startRow][$startCol] = " ";
                }
                $startRow += 1;
                $p1[$startRow][$startCol] = " ";
                $forward = true;
            }
        }

        return $p1;
    }

    function patternSpiral($length, $tmp, $doubleSide = false) {
        //PATTERN SPIRAL
        $p2 = $tmp;
        $startRow = 0;
        $startCol = 1;
        $size = $length - 1;
        $endRow = ($length - 1)/2;
        $endCol = ($length - 1)/2;
        $isRow = true;
        $layer = 0;
        $isPlus = true;
        $move = 'd';
        $moveMap = [
            'u' => [
                'next' => 'l',
                'isRow' => true,
                'isPlus' => false
            ],
            'd' => [
                'next' => 'r',
                'isRow' => true,
                'isPlus' => true
            ],
            'r' => [
                'next' => 'u',
                'isRow' => false,
                'isPlus' => true
            ],
            'l' => [
                'next' => 'd',
                'isRow' => false,
                'isPlus' => false
            ],
        ];
        $p2[$startRow][$startCol] = " ";
        if ($doubleSide) {
            $p2[$size - $startRow][$size - $startCol] = " ";
        }
        while ($startRow != $endRow || $startCol != $endCol) {
            while($startRow < $length || $startCol < $length) {
                $this->checkRow($startRow, $startCol, $isRow, $isPlus, 1);
                $limitLayer = 3;
                $pivot = $isRow ? $startRow : $startCol;
                $p2[$startRow][$startCol] = " ";
                if ($doubleSide) {
                    $p2[$size - $startRow][$size - $startCol] = " ";
                    $limitLayer = 1;
                    if ($startRow == $endRow && $startCol == $endCol) {
                        break;
                    }
                }
                $rule = $isPlus ? ($pivot + 1) == ($length - 1) : ($pivot - 1) == 0;
                if ($layer >= $limitLayer) {
                    $r = $startRow;
                    $c = $startCol;
                    $this->checkRow($r, $c, $isRow, $isPlus, 2);
                    $rule = ($p2[$r][$c] == " ");
                    if ($doubleSide) {
                        $rule = $rule && $r != $endRow +1 && $c != $endCol;
                    }
                }
                if ($rule) {
                    break;
                }
            }
            $move = $moveMap[$move]['next'];
            $layer += 1;
            $isRow = $moveMap[$move]['isRow'];
            $isPlus = $moveMap[$move]['isPlus'];
        }

        return $p2;
    }

    function patternNested($length, $tmp) {
        //PATTERN NESTED
        $p = $tmp;
        $startRow = 0;
        $startCol = 1;
        $endRow = 1;
        $endCol = $length - 2;
        $forward = false;
        $horizontal = false;
        $vertical = true;
        $p[$startRow][$startCol] = " ";
        $startRow += 1;

        while ($startRow != $endRow || $startCol != $endCol) {
            if ($horizontal) {
                $limit = $forward ? ($startCol + 3) : $length - 1;
                $right = ($startRow == 1) || ($startCol + 1 != $length - 1);
                for($i = $startCol; $right ? $i < $limit : $i > 0; $right ? $i++ : $i--) {
                    $startCol = $i;
                    $p[$startRow][$startCol] = " ";
                    if (!$right && ($startCol -1  != 0) && !$forward) {
                        if ($p[$startRow][$startCol - 2] == " ") {
                            break;
                        }
                    }
                }
                $vertical = true;
                $horizontal = false;
                $forward = $forward ? false : $right;
            } else if ($vertical) {
                $limit = $forward ? ($startRow - 3) : 0;
                $down = ($startRow == 1);
                for($i = $startRow; $down ? $i < $length - 1 : $i > $limit; $down ? $i++ : $i--) {
                    $startRow = $i;
                    $p[$startRow][$startCol] = " ";
                    if ($down && ($startRow + 1 != $length -1) && !$forward) {
                        if ($p[$startRow + 2][$startCol] == " ") {
                            break;
                        }
                    }
                }
                $horizontal = true;
                $vertical = false;
                $forward = $forward ? false : !$down;
            }
        }

        return $p;
    }
}
