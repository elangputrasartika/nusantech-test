<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 0;
            }
        </style>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="align-center container-fluid p-4">
            <div class="form-group">
                <label for="field-length">Length</label>
                <input type="number" min="1" required class="form-control" id="field-length" aria-describedby="length"
                       placeholder="Enter the length with this requirement Length = 4n - 1, where n is a positive integer (1, 2, 3, ...)"/>
                <small id="emailHelp" class="form-text text-muted">The output will be a square (Length X Length)</small>
                <div class="invalid-feedback" hidden>
                    Length value must equal to 4n - 1, where n is a positive integer (1, 2, 3, ...)
                </div>
            </div>
            <div id="div-output"></div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
        <script>
            $('#field-length').on("input",function() {
                let value = parseInt(this.value);

                if ((value + 1) % 4 === 0) {
                    document.getElementById('field-length').className = "form-control is-valid"
                    document.getElementsByClassName('invalid-feedback')[0].hidden = true

                    $.ajax({
                        type: 'GET',
                        url: `{{ route("pattern") }}`,
                        data: { length: value },
                        dataType: 'json',
                        success: function (data) {
                            $('#div-output').html('')
                            $.each(data, function(index, element) {
                                let value = ""
                                $('#div-output').append(`<div id="pattern-${index}"></div>`);
                                $.each(element, function(i, el) {
                                    value += el.join('') + "\r\n"
                                });
                                $(`#pattern-${index}`).append($('<pre>', {
                                    text: value
                                }));
                                $('#div-output').append(`<hr/>`);
                            });
                        }
                    });
                } else {
                    $('#div-output').html('')
                    document.getElementById('field-length').className = "form-control is-invalid"
                    document.getElementsByClassName('invalid-feedback')[0].hidden = false
                }
            });
        </script>
    </body>
</html>
